<!DOCTYPE html>
<html lang="en">
<head>
<?php include('bootstrap.php') ?>
<link rel="stylesheet" href="css/view.css">
</head>
<body>
<?php include('header.php') ?>
<div class="jumbotron bg-cover jumbotron-fluid">
  <div class="container">
    <h1 class="display-4">Fluid jumbotron</h1>
    <p class="lead">This is a modified jumbotron that occupies the entire horizontal space of its parent.</p>
  </div>
</div>
<div class="container-fluid">
<figure class="figure">
  <img src="image/clip.jpg" class="figure-img img-fluid rounded" alt="A generic square placeholder image with rounded corners in a figure.">
</figure>
</div>
<?php include('footer.php') ?>	
</body>
</html>