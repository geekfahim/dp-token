<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Login</title>
	<?php include("bootstrap.php") ?>
	<link rel="stylesheet" href="css/login.css">
</head>
<body>
<nav class="navbar navbar-expand navbar-light top-fixed">
<a href="index.php" class="navbar-brand m-l-30">
<img src="image/logo.png" alt="">
</a>
<a class="btn btn-primary btn-large ml-auto" href="index.php">
<i class="fa fa-home fa-2x">  
</i>Home
</a>
</nav>
	<div class="login">
	<h1>Login</h1>
    <form method="post">
    	<input type="text" name="u" placeholder="Username" required="required" />
        <input type="password" name="p" placeholder="Password" required="required" />
        <button type="submit" class="btn btn-primary btn-block btn-large">Log In</button>
    </form>
    <a href="" class="nav-link f-white">Create an account</a>
    <a href="" class="nav-link f-white">Forgot Password</a>
</div>
</body>
</html>