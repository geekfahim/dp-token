<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">

<?php include('bootstrap.php') ?>
<link rel="stylesheet" href="css/slider.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
<script src="js/slide.js"></script>
</head>
<body>
<?php include('header.php') ?>
<h1 class="clip">Hospital</h1>

<div class="container">
  <h2>Random Photos</h2>
   <section class="customer-logos slider">
      <div class="slide"><img src="image/slide.jpg">
      </div>
      <div class="slide"><img src="image/slide.jpg">
      </div>
      <div class="slide"><img src="image/slide.jpg">
      </div>
      <div class="slide"><img src="image/slide.jpg">
      </div>  
      <div class="slide"><img src="image/slide.jpg">
      </div>                    
   </section>
</div>

<div class="m-b-50"></div>

<div class="container">
	<div class="row">
		<div class="col-xs-6 col-md-12">
			<h1>This is heading</h1>
		</div>
		<div class="col-md-12">
			<p class="large-font">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
		</div>
	</div>
</div>
<?php include('footer.php') ?>
</body>
</html>