<!DOCTYPE html>
<html lang="en">
<head>
 <link rel="stylesheet" type="text/css" href="css/footer.css">
<?php include('bootstrap.php') ?>

</head>
<body>
<div class="m-t-100"></div>
	<div class="footer-top">
	   <div class="container">
	      <div class="social-icons">
				<i class="icon fa fa-twitter"></i>
				<i class="icon fa fa-facebook"></i>
				<i class="icon fa fa-google-plus"></i>
				<i class="icon fa fa-instagram"></i>
				<i class="icon fa fa-pinterest"></i>
				<i class="icon fa fa-youtube-play"></i>
		   </div>
	   </div>
	</div>
	
	<footer>
		<div class="footer" id="footer">
            <div class="container">
               <div class="row">
			
                <div class="col-lg-3 col-xs-12">
                  <h3>Payment Methods</h3>
                   <span>
                   		<span class="navbar-brand">
                       <img src="image/payment.png" class="img-fluid" alt="logo"> 
                      </span>
                   </span>
                </div>
                <div class="col-lg-3 col-xs-12">
                   <h3>Categories</h3>
                    <ul>
            						<li><a href="iconphp" class="nav-link">Ico Project</a></li>
            						<li><a href="health.php" class="nav-link">Health</a></li>
                        <li><a href="shop.php" class="nav-link">Super Shop</a></li>
                        <li><a href="it.php" class="nav-link">IT Institute</a></li>
                        <li><a href="hospital.php" class="nav-link">Hospital</a></li>
                        <li><a href="upcoming.php" class="nav-link">Upcoming Project</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-xs-12">
                    <h3>About Us</h3>
                    <span class="about">
                     <img src="image/about.gif" class="img-thumbnail" alt="">
                    </span>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                </div>                
              				<div class="col-lg-3 col-xs-12">
                          <div class="contact">
                              <h3>Contact</h3>
                            <ul>
                                <li><i class="fa fa-phone-square" aria-hidden="true"></i>&nbsp;&nbsp;+88018888888</li>
                                <li><i class="fa fa-envelope-square" aria-hidden="true"></i>&nbsp;&nbsp;support@tokenchai.com</li>
                                <li><i class="fa fa-globe" aria-hidden="true"></i>&nbsp;&nbsp;Head-Office:</li>
                                <li></li>
                                <li class="m-t-30">
                                <iframe width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed/v1/place?q=Cyberborg%20Bodyart%2C%20Bangka%2C%20Jakarta%2C%20Indonesia&key=AIzaSyCGz8WzqxQw1OwHWey3LCTjqKFG9feCxP4"></iframe>

                                </li>
                            </ul>
                          </div>
                      </div>
				
             </div>
          </div>
        </div>
	</footer>
	
	<div class="footer-bottom">
	    <p>Copyright © 2018. All Right Reserved <a href="www.dreamploy.com" class="f-white">Dreamploy</a></p>
	</div>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
</script>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
<script src="js/slide.js"></script>  
</body>
</html>