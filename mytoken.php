<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>MyToken</title>
<?php include('bootstrap.php') ?>
</head>
<body>
<?php include('header.php') ?>
	<div class="main">
    <h1 class="clip">My Token</h1>
	<div class="container">
<div class="table-responsive">	
	<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">S/L</th>
      <th scope="col">Project</th>
      <th scope="col">Code</th>
      <th scope="col">Quantity</th>
      <th scope="col">Host</th>
      <th scope="col">Price</th>
      <th scope="col">Yesterday</th>
      <th scope="col">Today</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Hospital</td>
      <td>Otto</td>
      <td>@mdo</td>
      <td>@mdo</td>
      <td>@mdo</td>
      <td>@mdo</td>
      <td>@mdo</td>
      <td>
<a href="sale.php">
<button type="button" class="btn btn-primary">
Sale
</button>
</a>
<a href="transfer.php">
<button type="button" class="btn btn-success">
Transfer
</button>
</a>
      </td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Jacob</td>
      <td>Thornton</td>
      <td>@fat</td>
      <td>Jacob</td>
      <td>Thornton</td>
      <td>@fat</td>
      <td>@fat</td>
      <td>
<a href="sale.php">
<button type="button" class="btn btn-primary">
Sale
</button>
</a>
<a href="transfer.php">
<button type="button" class="btn btn-success">
Transfer
</button>
</a>
      </td>      
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
      <td>@twitter</td>
      <td>
<a href="sale.php">
<button type="button" class="btn btn-primary">
Sale
</button>
</a>
<a href="transfer.php">
<button type="button" class="btn btn-success">
Transfer
</button>
</a>
      </td>      
    </tr>
    <tr>
      <th scope="row">4</th>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
      <td>@twitter</td>
      <td>
<a href="sale.php">
<button type="button" class="btn btn-primary">
Sale
</button>
</a>
<a href="transfer.php">
<button type="button" class="btn btn-success">
Transfer
</button>
</a>
      </td>      
    </tr>
    <tr>
      <th scope="row">5</th>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
      <td>Larry</td>
      <td>the Bird</td>
      <td>1080</td>
      <td>1085</td>
      <td>
<a href="sale.php">
<button type="button" class="btn btn-primary">
Sale
</button>
</a>
<a href="transfer.php">
<button type="button" class="btn btn-success">
Transfer
</button>	
</a>
  </td>      
    </tr>
  </tbody>
</table>
</div>

</div>
</div>

<?php include('footer.php') ?>
</body>
</html>