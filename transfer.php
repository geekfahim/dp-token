<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Sale || Transfer</title>
<?php include('bootstrap.php') ?>
<link rel="stylesheet" href="css/transfer.css">
</head>
<body>
	<?php include("header.php") ?>
	<div class="m-t-50"></div>
 <div class="container">
        	<div class="row">
               <div class="col-xs-4 col-md-6 item-photo">
                    <img src="image/health.jpg" class="img-fluid" />
                </div>
     <div class="col-xs-5 offset-xs-1 col-md-5 offset-md-1" style="border:0px solid gray">
                    <!-- Datos del vendedor y titulo del producto -->
                    <h3>Health Project</h3>    
                    <h5 style="color:#337ab7">Product Code:
						<span>pra-12</span>
                    </h5>        
                    <!-- Precios -->
                    <h6 class="title-price">Price</h6>
                    <h3 style="margin-top:0px;">BDT 1080</h3>         
                    <div class="section" style="padding-bottom:20px;">
                        <h6 class="title-attr"><small>Quantity</small></h6><br>			                   
                        <div>
                            <div class="btn-minus">
                            <i class="fa fa-minus" aria-hidden="true"></i>
                            </div>
                            <input value="1" />
                            <div class="btn-plus">
                            	<i class="fa fa-plus" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>               
	   <!-- Button trigger modal -->
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
				  Submit
				</button>

				<!-- Modal -->
				<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="exampleModalLabel">Please Confirm Password</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
						<div class="input-group mb-3">
						  <div class="input-group-prepend">
						    <button class="btn btn-outline-secondary" type="button">
						    <i class="fa fa-lock" aria-hidden="true"></i>&nbsp;Password
						    </button>
						  </div>
						  <input type="password" class="form-control" placeholder="Enter Your Password" aria-label="" aria-describedby="basic-addon1">
						</div>
			      	 </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				        <button type="button" class="btn btn-primary">Submit</button>
				      </div>
				    </div>
				  </div>
				</div>       
                </div>                                     		
            </div>
        </div>        
	<?php include('footer.php') ?>
<script src="js/quantity.js"></script>
</body>
</html>