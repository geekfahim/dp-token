<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>DP || Token</title>
<?php include('bootstrap.php') ?>

</head>
<body>
<?php include('header.php') ?>	
<!-- <div class="jumbotron cover jumbotron-fluid">
  <div class="container">
    <h1 class="display-4">Fluid jumbotron</h1>
    <p class="lead">This is a modified jumbotron that occupies the entire horizontal space of its parent.</p>
  </div>
</div> -->
<div class="main">
    <h1 class="clip">Token</h1>
	<div class="container-fluid">
	<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">S/L</th>
      <th scope="col">Project</th>
      <th scope="col">Code</th>
      <th scope="col">Quantity</th>
      <th scope="col">Place</th>
      <th scope="col">Price</th>
      <th scope="col">Selling Date</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Hospital</td>
      <td>Otto</td>
      <td>@mdo</td>
      <td>@mdo</td>
      <td>@mdo</td>
      <td>@mdo</td>
      <td>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
View  
</button>
      </td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Jacob</td>
      <td>Thornton</td>
      <td>@fat</td>
      <td>Jacob</td>
      <td>Thornton</td>
      <td>@fat</td>
      <td>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal1">
View  
</button>

      </td>      
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
      <td>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal2">
View  
</button>
      </td>      
    </tr>
    <tr>
      <th scope="row">4</th>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
      <td>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal3">
View  
</button>
      </td>      
    </tr>
    <tr>
      <th scope="row">5</th>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
      <td>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal4">
View  
</button>
      </td>      
    </tr>
  </tbody>
</table>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Hospital</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <h5>Project Name:&nbsp;Health </h5>
          <h5>Code:&nbsp;HS004 </h5>
          <h5>Place:&nbsp;Sherpur </h5>
          <h5>Price:&nbsp;1080BDT</h5>
          <h5>Selling Date:&nbsp;20.02.2018</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="chekout.php"><button type="button" class="btn btn-primary">Buy Now</button></a>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">IT</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <h5>Project Name:&nbsp;IT </h5>
          <h5>Code:&nbsp;HS004 </h5>
          <h5>Place:&nbsp;Sherpur </h5>
          <h5>Price:&nbsp;1080BDT</h5>
          <h5>Selling Date:&nbsp;20.02.2018</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="checkout.php"><button type="button" class="btn btn-primary">Buy Now</button></a>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">IT</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <h5>Project Name:&nbsp;IT </h5>
          <h5>Code:&nbsp;HS004 </h5>
          <h5>Place:&nbsp;Sherpur </h5>
          <h5>Price:&nbsp;1080BDT</h5>
          <h5>Selling Date:&nbsp;20.02.2018</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="checkout.php"><button type="button" class="btn btn-primary">Buy Now</button></a>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">IT</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <h5>Project Name:&nbsp;IT </h5>
          <h5>Code:&nbsp;HS004 </h5>
          <h5>Place:&nbsp;Sherpur </h5>
          <h5>Price:&nbsp;1080BDT</h5>
          <h5>Selling Date:&nbsp;20.02.2018</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="checkout.php"><button type="button" class="btn btn-primary">Buy Now</button></a>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Caption</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <h5>Project Name:&nbsp;Health </h5>
          <h5>Code:&nbsp;HS004 </h5>
          <h5>Place:&nbsp;Sherpur </h5>
          <h5>Price:&nbsp;1080BDT</h5>
          <h5>Selling Date:&nbsp;20.02.2018</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="checkout.php"><button type="button" class="btn btn-primary">Buy Now</button></a>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="exampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Caption</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <h5>Project Name:&nbsp;Health </h5>
          <h5>Code:&nbsp;HS004 </h5>
          <h5>Place:&nbsp;Sherpur </h5>
          <h5>Price:&nbsp;1080BDT</h5>
          <h5>Selling Date:&nbsp;20.02.2018</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="checkout.php"><button type="button" class="btn btn-primary">Buy Now</button></a>
      </div>
    </div>
  </div>
</div>
</div>

<?php include('footer.php') ?>
</body>
</html>