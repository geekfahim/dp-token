<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>DP || Token</title>
  <?php include("bootstrap.php") ?>
</head>
<body>
<?php include("header.php") ?>
<h1 class="clip">Dp Token</h1>
<div class="m-b-50"></div>
<div class="maincontent">
  <div class="container">
<div class="row">
<div class="card-deck">   
  <div class="card">
    <img class="card-img-top" src="image/hospital.jpg" alt="Card image cap">
    <div class="card-body">
      <h5 class="card-title">Token Market</h5>
      <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
     
    </div>
  </div>
  <div class="card">
    <img class="card-img-top" src="image/training.jpg" alt="Card image cap">
    <div class="card-body">
      <h5 class="card-title">My Token</h5>
      <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>     
    </div>
  </div>
  <div class="card">
    <img class="card-img-top" src="image/shop.jpg" alt="Card image cap">
    <div class="card-body">
      <h5 class="card-title">Ico Project</h5>
      <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
     
    </div>
  </div>
</div>
</div>
<div class="m-t-50"></div>
<div class="row">
<div class="card-deck">   
  <div class="card">
    <img class="card-img-top" src="image/hospital.jpg" alt="Card image cap">
    <div class="card-body">
      <h5 class="card-title">Hospital</h5>
      <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
     
    </div>
  </div>
  <div class="card">
    <img class="card-img-top" src="image/training.jpg" alt="Card image cap">
    <div class="card-body">
      <h5 class="card-title">IT Institute</h5>
      <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>     
    </div>
  </div>
  <div class="card">
    <img class="card-img-top" src="image/shop.jpg" alt="Card image cap">
    <div class="card-body">
      <h5 class="card-title">Supper Shop</h5>
      <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
     
    </div>
  </div>
  <div class="card">
    <img class="card-img-top" src="image/shop.jpg" alt="Card image cap">
    <div class="card-body">
      <h5 class="card-title">Upcoming Project</h5>
      <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
     
    </div>
  </div>
</div>  
</div>

  </div>
</div>
<?php include("footer.php") ?>
</body>
</html>