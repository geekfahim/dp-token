<!DOCTYPE html>
<html lang="en">
<head>
<?php include('bootstrap.php') ?>
</head>
<body>
<?php include('header.php') ?>
<div class="container">
  <header class="page-header">
    <h1>Dark Responsive Timeline with Bootstrap</h1>
  </header>
  
  <ul class="timeline">
    <li><div class="tldate">March 2018</div></li>
    
    <li>
      <div class="tl-circ">
      	<i class="fa fa-check" aria-hidden="true"></i>
      </div>
      <div class="timeline-panel">
        <div class="tl-heading">
          <h4>Surprising Headline Right Here</h4>
          <p><small class="text-muted"><i class="fa fa-calendar" aria-hidden="true"></i> 3 hours ago</small></p>
        </div>
        <div class="tl-body">
          <p>Lorem Ipsum and such.</p>
        </div>
      </div>
    </li>
    
    <li class="timeline-inverted">
      <div class="tl-circ">
      	<i class="fa fa-check" aria-hidden="true"></i>
      </div>
      <div class="timeline-panel">
        <div class="tl-heading">
          <h4>Breaking into Spring!</h4>
          <p><small class="text-muted"><i class="fa fa-calender"></i> 4/07/2018</small></p>
        </div>
        <div class="tl-body">
          <p>Hope the weather gets a bit nicer...</p>
            
          <p>Y'know, with more sunlight.</p>
        </div>
      </div>
    </li>
    
    <li><div class="tldate">May 2018</div></li>
    
    <li>
      <div class="tl-circ">
      	<i class="fa fa-check" aria-hidden="true"></i>
      </div>
      <div class="timeline-panel">
        <div class="tl-heading">
          <h4>New Apple Device Release Date</h4>
          <p><small class="text-muted"><i class="fa fa-calendar" aria-hidden="true"></i> 3/22/2018</small></p>
        </div>
        <div class="tl-body">
          <p>In memory of Steve Jobs.</p>
        </div>
      </div>
    </li>
    <li class="timeline-inverted">
    <div class="tl-circ">
      	<i class="fa fa-check" aria-hidden="true"></i>
      </div>
      <div class="timeline-panel">
        <div class="tl-heading">
          <h4>No icon here</h4>
          <p><small class="text-muted"><i class="fa fa-clock"></i> 3/16/2018</small></p>
        </div>
        <div class="tl-body">
          <p>Here you'll find some beautiful photography for your viewing pleasure, courtesy of <a href="#">lorempixel</a>.</p>
          
          <p><img src="image/color.jpg" alt="lorem pixel"></p>
        </div>
      </div>
    </li>
    <li>
      <div class="tl-circ">
      	<i class="fa fa-check" aria-hidden="true"></i>
      </div>
      <div class="timeline-panel">
        <div class="tl-heading">
          <h4>Some Important Date!</h4>
          <p><small class="text-muted"><i class="fa fa-calendar" aria-hidden="true"></i> 3/03/2018</small></p>
        </div>
        <div class="tl-body">
          <p>Write up a quick summary of the event.</p>
        </div>
      </div>
    </li>
    <li>
      <div class="timeline-panel noarrow">
        <div class="tl-heading">
          <h4>Secondary Timeline Box</h4>
          <p><small class="text-muted"><i class="fa fa-calendar" aria-hidden="true"></i> 3/01/2018</small></p>
        </div>
        <div class="tl-body">
          <p>This might be a follow-up post with related info. Maybe we include some extra links, tweets, user comments, photos, etc.</p>
        </div>
      </div>
    </li>
    
    <li><div class="tldate">June 2018</div></li>
    
    <li class="timeline-inverted">
      <div class="tl-circ">
      	<i class="fa fa-check" aria-hidden="true"></i>
      </div>
      <div class="timeline-panel">
        <div class="tl-heading">
          <h4>The Winter Months</h4>
          <p><small class="text-muted"><i class="fa fa-calendar" aria-hidden="true"></i> 02/23/2018</small></p>
        </div>
        <div class="tl-body">
          <p>Gee time really flies when you're having fun.</p>
        </div>
      </div>
    </li>
    <li>
      <div class="tl-circ">
      	<i class="fa fa-check" aria-hidden="true"></i>
      </div>
      <div class="timeline-panel">
        <div class="tl-heading">
          <h4>Yeah we're pretty much done here</h4>
          <p><small class="text-muted"><i class="fa fa-calendar" aria-hidden="true"></i> 02/11/2018</small></p>
        </div>
        <div class="tl-body">
          <p>Wasn't this fun though?</p>
        </div>
      </div>
    </li>

  </ul>
  </div>
<?php include('footer.php') ?>	
</body>
</html>